/**
 * 
 *@author Azadeh Ahmadi 
 * 
 */
package test;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
public class RpsApplication extends Application {
	private RpsGame round=new RpsGame();
	@Override
	public void start(Stage stage) {
		Group root = new Group(); 
		HBox buttons= new HBox();
		Button button1 = new Button("Rock");
		Button button2 = new Button("Scissors");
		Button button3 = new Button("Paper");
		buttons.getChildren().addAll(button1, button2, button3);
		HBox textfields= new HBox();
		
		TextField tf1 = new TextField("wellcome!");
		TextField tf2 = new TextField("Wins:");
		TextField tf3 = new TextField("losses:");
		TextField tf4 = new TextField("Ties:");
		textfields.getChildren().addAll(tf1,tf2,tf3,tf4);
		tf1.setPrefWidth(200);
		VBox overall=new VBox();
		
		overall.getChildren().addAll(buttons,textfields);
		root.getChildren().add(overall);
		
		RpsChoice actionObject1 = new RpsChoice(tf1,tf2,tf3,tf4,"Rock",this.round);
		RpsChoice actionObject2 = new RpsChoice(tf1,tf2,tf3,tf4,"Scissors",this.round);
		RpsChoice actionObject3 = new RpsChoice(tf1,tf2,tf3,tf4,"Paper",this.round);
		button1.setOnAction(actionObject1);
		button2.setOnAction(actionObject2);
		button3.setOnAction(actionObject3);
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		stage.show(); 
		}
	 public static void main(String[] args) {
        Application.launch(args);
    }
}    

