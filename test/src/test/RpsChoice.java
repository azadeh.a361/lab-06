
/**
 * 
 *@author Azadeh Ahmadi 
 * 
 */
package test;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;
public  class RpsChoice implements EventHandler<ActionEvent> {
	private TextField message;
	private TextField wins;
	private TextField losses;
	private TextField ties;
	private String input;
	private RpsGame gameround=new RpsGame();
	
	public RpsChoice (TextField message ,TextField wins ,TextField losses,TextField ties,String  input ,RpsGame gameround) {
this.message=message;
this.wins=wins;
this.losses=losses;
this.ties=ties;
this.input=input;
this.gameround=gameround;

}
	public void handle (ActionEvent e) {
		
		this.message.setText(this.gameround.playRound(this.input));
		this.wins.setText("wins"+this.gameround.getWins());
		this.losses.setText("losses"+this.gameround.getLosses());
		this.ties.setText("ties"+this.gameround.getTies());
	}
	
	}

