/**
 * 
 *@author Azadeh Ahmadi 
 * 
 */
package test;
import java.util.*;
public class RpsGame {
private int wins=0;
	private int losses=0;
	private int ties=0;
	Random x=new Random();
	public RpsGame() {
		this.wins=0;
		this.losses=0;
		this.ties=0;
	}
	
	public int getLosses() {
		return this.losses;
	}
	public int getWins() {
		return this.wins;
	}
	
	public int getTies() {
		return this.ties;
	}
	
	public String playRound(String input) {
		 int comChoice=x.nextInt(3);
		 String computer="";
		 String result="";
		 if (comChoice==0)
				computer="Rock";
			 else if (comChoice==1)
	computer="Paper";
			 else 
				computer="Scissors";
				
		if (input.equals(computer)) {
		
		this.ties=this.ties+1;
		 result ="Ties";}
		else
		if ((input.equals("Rock")&& computer.equals("Scissors"))||(input.equals("Scissors")&& computer.equals("Paper"))||(input.equals("Paper")&& computer.equals("Rock")))
				{this.losses=this.losses+1;
				  result="Lost";}
		else
			{this.wins=this.wins+1;
			 result = "Won";}
		
		return "computer played " +(computer)+" and "+(result)+".";
	}}



